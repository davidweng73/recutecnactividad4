using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowControl : MonoBehaviour
{
    Animator _windowAnim;
    private void OnTriggerEnter(Collider other)
    {
        _windowAnim.SetBool("isOpening2",true);
    }
    private void OnTriggerExit (Collider other)
    {
        _windowAnim.SetBool("isOpening2", false);
    }
    // Start is called before the first frame update
    void Start()
    {
        _windowAnim = this.transform.parent.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
