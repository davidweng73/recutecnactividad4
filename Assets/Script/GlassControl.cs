using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassControl : MonoBehaviour
{
    Animator _glassAnim;
    private void OnTriggerEnter(Collider other)
    {
        _glassAnim.SetBool("isOpening3",true);
    }
    private void OnTriggerExit (Collider other)
    {
        _glassAnim.SetBool("isOpening3", false);
    }
    // Start is called before the first frame update
    void Start()
    {
          _glassAnim = this.transform.parent.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
